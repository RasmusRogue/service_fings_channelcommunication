package com.fullfacing.communicationChannel

import java.util.UUID

import akka.actor.{Actor, ActorSystem, Props}
import akka.routing.FromConfig
import com.fullfacing.Imports._
import com.fullfacing.common.fings.context.{MongoConnectionContext, MongoContext}
import com.fullfacing.common.fings.message._
import com.fullfacing.common.fings.model.ECommunicationChannel
import com.fullfacing.framework.AtomizerFormats.default
import com.fullfacing.framework.DefaultAtomizers._
import com.fullfacing.framework.Security._
import com.fullfacing.message.Status._
import com.fullfacing.message._
import com.fullfacing.queue.RabbitMQ._
import com.fullfacing.queue.{ActorSubscribeRequest, MessageQueue, Publish}
import com.mongodb.casbah.MongoDB
import com.rabbitmq.client.{Channel, Connection}
import org.slf4j.LoggerFactory
import com.fullfacing.message.User

import scala.language._
import scalaz.Success

/**
  * Project: CommunicationChannelService
  * Created on 24/2/2016 by Petrus de Kock <petrus@zapop.com>
  * Adapted from the MST, Ticketing Seat Planner
  */


object Main extends App with MongoConnectionContext {
  val logger = LoggerFactory.getLogger("CommunicationChannelLogger")
  try {
    val system = ActorSystem.create("actorsystem")

    // these are used but the ActorSubscribe to convert the incoming byte array into a concrete type that you can use
    implicit def arrayToGetAllRequest(array: Array[Byte]): Request[GetAllRequest] = array.deatomized[Request[GetAllRequest]]
    implicit def arrayToGetByIdRequest(array: Array[Byte]): Request[GetByIdRequest] = array.deatomized[Request[GetByIdRequest]]
    implicit def arrayToSaveRequest(array: Array[Byte]): Request[SaveRequest[CommunicationChannel]] = array.deatomized[Request[SaveRequest[CommunicationChannel]]]
    implicit def arrayToUpdateRequest(array: Array[Byte]): Request[UpdateRequest[CommunicationChannel]] = array.deatomized[Request[UpdateRequest[CommunicationChannel]]]

    implicit val queue = new MessageQueue[Channel, Connection]

    val communicationChannelActor = system.actorOf(Props(new CommunicationChannelActor(_db)(queue)).withRouter(FromConfig()), "CommunicationChannelActor")
    queue.subscribeRequests(
      ActorSubscribeRequest[Request[GetAllRequest]](Topics.CommunicationChannel.getAllRequest, communicationChannelActor),
      ActorSubscribeRequest[Request[GetByIdRequest]](Topics.CommunicationChannel.getByIdRequest, communicationChannelActor),
      ActorSubscribeRequest[Request[SaveRequest[CommunicationChannel]]](Topics.CommunicationChannel.saveRequest, communicationChannelActor),
      ActorSubscribeRequest[Request[UpdateRequest[CommunicationChannel]]](Topics.CommunicationChannel.updateRequest, communicationChannelActor)
    )
  } catch {
    case t: Throwable => logger.error(s"${Console.RED}CommunicationChannel service failed: ${t.getMessage}${Console.RESET}", t)
  }
}

class CommunicationChannelActor(val _db: MongoDB)(queue: MessageQueue[Channel, Connection]) extends Actor with MongoContext {

  import com.fullfacing.common.fings.dao.FingsMongoDAO._

  implicit val q = queue
  val logger = LoggerFactory.getLogger(classOf[CommunicationChannelActor])

  // If the mapping from entity to message changes in future make changes to these two
  //  val mapECommunicationChannelToCommunicationChannel: ECommunicationChannel => CommunicationChannel = ECommunicationChannel =>
  def mapECommunicationChannelToCommunicationChannel(e: ECommunicationChannel): CommunicationChannel =
    CommunicationChannel(e.id,
      e.communicationChannelUUID,
      e.name,
      e.description,
      e.properties,
      e.createdDate)

  def mapCommunicationChannelToECommunicationChannel(c: CommunicationChannel): ECommunicationChannel =
    ECommunicationChannel(c.communicationChannelUUID,
      c.name,
      c.description,
      c.properties,
      c.createdDate,
      c.id)

  override def receive: Receive = {
    case Request(uuid, tokenized, meta) =>
      logger.info(s"${Console.WHITE}RequestIN(${Console.RED}$uuid${Console.WHITE}):::  ${Console.MAGENTA}${tokenized.value}${Console.RESET}")
      validate(tokenized) match {

        case Success(GetAllRequest(user: Option[User])) =>
          GetAllResponse(communicationChannel.list().map(mapECommunicationChannelToCommunicationChannel).toList)
            .toResponse(OK, "OK")(uuid).publish(Topics.CommunicationChannel.getAllResponse)

        case Success(GetByIdRequest(ids, user: Option[User])) =>
          val eCommunicationChannelList = ids.flatMap(id =>
            communicationChannel.headOption(ECommunicationChannel.id === Some(id)).map(mapECommunicationChannelToCommunicationChannel).toList)
          GetByIdResponse(eCommunicationChannelList)
            .toResponse(OK, "OK")(uuid).publish(Topics.CommunicationChannel.getByIdResponse)

        case Success(SaveRequest(communicationChannelToSave: CommunicationChannel, user: Option[User])) =>
          (communicationChannel.headOption(ECommunicationChannel.communicationChannelUUID === communicationChannelToSave.communicationChannelUUID) match {
            case Some(existingCommunicationChannel) =>
              SaveResponse(None).toResponse(BAD_REQUEST, "CommunicationChannel exists, use UpdateRequest instead")
            case None =>
              val savedCommunicationChannel = communicationChannel += mapCommunicationChannelToECommunicationChannel(communicationChannelToSave)
              SaveResponse[CommunicationChannel](Some(communicationChannelToSave.copy(id = savedCommunicationChannel.id))).toResponse(CREATED, "CommunicationChannel Saved")
          }) (uuid).publish(Topics.CommunicationChannel.saveResponse)

        case Success(UpdateRequest(communicationChannelToUpdate: CommunicationChannel, user: Option[User])) =>
          communicationChannel.headOption(ECommunicationChannel.communicationChannelUUID === communicationChannelToUpdate.communicationChannelUUID).map { _ =>
            communicationChannel := mapCommunicationChannelToECommunicationChannel(communicationChannelToUpdate)
            UpdateResponse(Some(communicationChannelToUpdate)).toResponse(ACCEPTED, "CommunicationChannel Updated")
          }.getOrElse(UpdateResponse(None).toResponse(BAD_REQUEST, "CommunicationChannel does not exist, use SaveRequest instead"))(uuid).publish(Topics.CommunicationChannel.updateResponse)

        case x =>
          println("Error on recieved message: " + x)
          Publish("NO TOPIC EVER", "Should never get HERE something is afoot".atomized)
      }

    case x =>
      println("something went wrong: " + x) // got data type not looking for
  }

}
